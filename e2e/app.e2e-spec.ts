import { MhrTechnicalTestPage } from './app.po';

describe('mhr-technical-test App', function() {
  let page: MhrTechnicalTestPage;

  beforeEach(() => {
    page = new MhrTechnicalTestPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
