import { Component } from '@angular/core';
import { CommonService } from './services/common.service';
import { ArticleViewModel } from './models/article-view-model';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.css'],
    providers: [CommonService]
})


export class AppComponent {
    public articles: ArticleViewModel[] = [
        new ArticleViewModel('article-1', 'Heading One'),
        new ArticleViewModel('article-2', 'Heading Two'),
        new ArticleViewModel('article-3', 'Heading Three')
    ];
}
