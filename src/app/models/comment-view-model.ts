export class CommentViewModel {

  private _id: string = '';
  private _articleId: string = '';
  private _posterName: string = '';
  private _text: string = '';

  public get id(): string {
    return this._id;
  }

  public set id(value: string) {
    this._id = value;
  }

  public get articleId(): string {
    return this._articleId;
  }

  public set articleId(value: string) {
    this._articleId = value;
  }

  public get posterName(): string {
    return this._posterName;
  }

  public set posterName(value: string) {
    this._posterName = value;
  }

  public get text(): string {
    return this._text;
  }

  public set text(value: string) {
    this._text = value;
  }

  constructor(id?: string, articleId?: string, posterName?: string, text?: string) {
    this._id = id || '';
    this._articleId = articleId || '';
    this._posterName = posterName || '';
    this._text = text || '';
  }
}
