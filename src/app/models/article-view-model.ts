export class ArticleViewModel {

  private _id: string = '';
  private _title: string = '';
  private _likesCount: number = 0;

  public get id(): string {
    return this._id;
  }

  public get title(): string {
    return this._title;
  }

  public set title(value: string) {
    this._title = value;
  }

  public get likesCount(): number {
    return this._likesCount;
  }

  public set likesCount(value: number) {
    this._likesCount = value;
  }

  constructor(id?: string, title?: string) {
    this._id = id || '';
    this._title = title || '';
  }
}
