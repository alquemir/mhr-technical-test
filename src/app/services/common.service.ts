import { Injectable } from '@angular/core';
import { Subject } from 'rxjs/Subject';
import { CommentViewModel } from '../models/comment-view-model';

@Injectable()
export class CommonService {

  private _creatingCommentSource: any = null;
  private _creatingComment$: any = null;

  private _commentCreatedSource: any = null;
  private _commentCreated$: any = null;

  private _editCommentSource: any = null;
  private _editComment$; any = null;

  private _commentUpdatedSource: any = null;
  private _commentUpdated$: any = null;

  public get creatingComment$(): any {
    return this._creatingComment$;
  }

  public get commentCreated$(): any {
    return this._commentCreated$;
  }

  public get editComment$(): any {
    return this._editComment$;
  }

  public get commentUpdated$(): any {
    return this._commentUpdated$;
  }

  constructor() {
    this._creatingCommentSource = new Subject();
    this._creatingComment$ = this._creatingCommentSource.asObservable();

    this._commentCreatedSource = new Subject();
    this._commentCreated$ = this._commentCreatedSource.asObservable();

    this._editCommentSource = new Subject();
    this._editComment$ = this._editCommentSource.asObservable();

    this._commentUpdatedSource = new Subject();
    this._commentUpdated$ = this._commentUpdatedSource.asObservable();
  }

  public createComment(articleId: string): void {
    this._creatingCommentSource.next(articleId);
  }

  public commentCreated(comment: CommentViewModel) {
    this._commentCreatedSource.next(comment);
  }

  public editComment(comment: CommentViewModel) {
    this._editCommentSource.next(comment);
  }

  public commentUpdated(comment: CommentViewModel) {
    this._commentUpdatedSource.next(comment);
  }
}
