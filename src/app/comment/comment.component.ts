import { Component, Input, ViewChild, ElementRef, OnInit, AfterViewChecked, HostListener } from '@angular/core';
import { CommentViewModel } from '../models/comment-view-model';
import { CommonService } from '../services/common.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';


@Component({
  selector: 'app-comment',
  templateUrl: './comment.component.html',
  styleUrls: ['./comment.component.css']
})


export class CommentComponent implements OnInit, AfterViewChecked {
  @ViewChild('container')
  public containerElement: ElementRef = null;

  @ViewChild('commentForm')
  public commentForm: FormGroup = null;

  private _backgroundElement: HTMLElement = null;
  private _maxCharacters: number = 500;
  private _commonService: CommonService = null;
  private _isUpdate: boolean = false;

  public model: CommentViewModel = new CommentViewModel();
  public isVisible: boolean = false;
  public countMessage = '';
  public countMessageCss = '';

  constructor(commonService: CommonService) {
    this._commonService = commonService;
  }

  ngOnInit() {
    this.createBackgroundElement();
    this.centerContainerElement();

    this._commonService.creatingComment$.subscribe((articleId) => {
      this.model.articleId = articleId;
      this.model.posterName = '';
      this.model.text = '';
      this.commentForm.reset();
      this._isUpdate = false;
      this.show();
    });

    this._commonService.editComment$.subscribe((comment) => {
      this.model.id = comment.id;
      this.model.posterName = comment.posterName;
      this.model.text = comment.text;

      this.copyFromModelToFormControls();

      this._isUpdate = true;
      this.show();
    });

    this.commentForm = this.createCommentFormGroup();
  }

  private createCommentFormGroup(): FormGroup {
    return new FormGroup({
      posterName: new FormControl('', [Validators.required]),
      text: new FormControl('', [Validators.required, this.characterCountValidator.bind(this)])
    });
  }

  public characterCountValidator(control: FormControl): any {
    return this.getTotalCharacters(control.value) >= this._maxCharacters
      ? { characterCount: { valid: false } }
      : null;
  }


  ngAfterViewChecked() {
    this.commentForm.valueChanges.subscribe((data) => {
      let totalCharacters = this.getTotalCharacters(data.text);
      this.countMessage = `${totalCharacters} characters out of ${this._maxCharacters}`;
    });
  }

  @HostListener('window:resize', ['$event'])
  onResize(e) {
    this.centerContainerElement();
  }

  private createBackgroundElement() {
    this._backgroundElement = document.createElement('div');
    this._backgroundElement.className = 'fade';
    document.body.insertBefore(this._backgroundElement, document.body.firstChild);
    this.hideBackgroundElement();
  }

  private calculateContainerElementHorizontalOffset(): number {
    return (window.innerWidth - this.containerElement.nativeElement.offsetWidth) / 2;
  }

  private calculateContainerElementVerticalOffset(): number {
    return (window.innerHeight - this.containerElement.nativeElement.offsetHeight) / 2;
  }

  private centerContainerElement(): void {
    this.containerElement.nativeElement.style.top = `${this.calculateContainerElementVerticalOffset()}px`;
    this.containerElement.nativeElement.style.left = `${this.calculateContainerElementHorizontalOffset()}px`;
  }

  private showBackgroundElement(): void {
    this._backgroundElement.style.display = 'block';
  }

  private hideBackgroundElement(): void {
    this._backgroundElement.style.display = 'none';
  }

  public validate(controlName: string, errorName: string): boolean {
    return this.commentForm.contains(controlName)
      && this.commentForm.controls[controlName].touched
      && this.commentForm.controls[controlName].errors != null
      && this.commentForm.controls[controlName].errors[errorName];
  }

  public close(e) {
    e.preventDefault();
    this.hide();
  }

  public show(): void {
    this.showBackgroundElement();
    this.isVisible = true;

    setTimeout(() => this.centerContainerElement(), 0);
  }

  public hide(): void {
    this.hideBackgroundElement();
    this.isVisible = false;
  }

  private copyFromModelToFormControls(): void {
    this.commentForm.value['posterName'] = this.model.posterName;
    this.commentForm.value['text'] = this.model.text;
  }

  private copyFromFormControlsToModel(): void {
    this.model.posterName = this.commentForm.value['posterName'];
    this.model.text = this.commentForm.value['text'];
  }

  public submitComment() {

    this.copyFromFormControlsToModel()

    if (!this._isUpdate) {
      this._commonService.commentCreated(this.model);
    } else {
      this._commonService.commentUpdated(this.model);
    }

    this.hide();
  }

  private getTotalCharacters(source: string): number {
    if (source == null) return 0;
    let matches = source.match(/./g);
    return matches != null ? matches.length : 0;
  }

}
