import { Component, Input, OnInit } from '@angular/core';
import { ArticleViewModel } from '../models/article-view-model';
import { CommonService } from '../services/common.service';
import { CommentViewModel } from '../models/comment-view-model';

@Component({
  selector: 'app-article',
  templateUrl: './article.component.html',
  styleUrls: ['./article.component.css']
})

export class ArticleComponent implements OnInit {

  @Input()
  public model: ArticleViewModel = new ArticleViewModel();

  @Input()
  public cssClass: string = "odd";
  public comments: CommentViewModel[] = [];

  private _commonService: CommonService = null;

  constructor(commonService: CommonService) {
    this._commonService = commonService;
  }

  ngOnInit() {
    this._commonService.commentCreated$.subscribe((comment) => {
      if (this.model.id == comment.articleId) {
        let commentToAdd = new CommentViewModel(
          `${comment.articleIdd}-comment-${this.comments.length}`,
          comment.articleId,
          comment.posterName,
          comment.text
        );

        this.comments.push(commentToAdd);
      }
    });

    this._commonService.commentUpdated$.subscribe((comment) => {
      if (this.model.id == comment.articleId) {
        let commentToUpdate = this.findCommentById(comment.id);
        commentToUpdate.posterName = comment.posterName;
        commentToUpdate.text = comment.text;
      }
    });

  }

  private findCommentById(id: string): CommentViewModel {
    return this.comments.find((comment) => comment.id == id);
  }

  public editComment(e, commentId: string) {
    e.preventDefault();
    let comment = this.findCommentById(commentId);
    this._commonService.editComment(comment);
  }

  public removeComment(e, commentId: string) {
    e.preventDefault();
    let index = this.comments.findIndex((comment) => comment.id == commentId);
    this.comments.splice(index, 1);
  }

  public upvote(e): void {
    e.preventDefault();

    if (this.model.likesCount >= 10) {
      this.model.likesCount = 10;
      return;
    }

    this.model.likesCount++;
  }

  public downvote(e): void {
    e.preventDefault();

    if (this.model.likesCount <= 0) {
      this.model.likesCount = 0;
      return;
    }

    this.model.likesCount--;
  }

  public addComment(e) {
    e.preventDefault();
    this._commonService.createComment(this.model.id);
  }

  public getCssClass(): string {
    return `article ${this.cssClass}`;
  }
}
